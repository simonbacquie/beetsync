#!/usr/bin/env php
<?php

// rsync -a --files-from=rsynclist.txt /home/simon/Music/beetslibrary/ 
// /home/simon/Development/rsync_test/destination3
// find $PWD
// ^ gets all the files recursively with absolute paths

// define("SYNCPATH", "/media/simon/CARMUSIC32G/");
define("BEETSPATH", "/home/simon/Music/beetslibrary/");
// define("SYNCPATH", "/home/simon/Development/music_sync_test/");
define("SYNCPATH", "/media/simon/CARMUSIC32G/synced/");

if (!is_writable(SYNCPATH))
	die("Sync path is not writable! Exiting...\n");

$genres = array(
	// "Drum & Bass",
	"Freestyle",
	"Happy Hardcore",
	// "Breakbeat",
	// "Trance",
	// "Terror EBM"
);

$remove_queries = array(
	"Genocide God Module"
);

file_put_contents('rsynclist.txt', "");

foreach ($genres as $genre) {
	$results = `beet ls -p genre:"$genre"`;
	$results = str_replace(BEETSPATH, "", $results);
	file_put_contents('rsynclist.txt', $results, FILE_APPEND | LOCK_EX);
}

foreach ($remove_queries as $remove_query) {
	$results = `beet ls -p $remove_query`;
	$results = str_replace(BEETSPATH, "", $results);
	file_put_contents('remove_files.txt', $results, FILE_APPEND | LOCK_EX);
}

shell_exec("cat /home/simon/Development/musicrsync/rsynclist.txt | sort | uniq > rsynclist2.txt");
shell_exec("cat /home/simon/Development/musicrsync/remove_files.txt | sort | uniq > remove_files2.txt");

$all_files = file('rsynclist2.txt');
$bad_files = file('remove_files2.txt');
$good_files = array_diff($all_files, $bad_files);
print_r($bad_files);

// print_r($file);
// file_put_contents('rsynclist3.txt', $good_files);
// die();

shell_exec("rsync -a --files-from=rsynclist2.txt " . BEETSPATH . " " . SYNCPATH);

die("Done.");

foreach ($genres as $genre) {
	$results = `beet ls -f '\$artist|\$album|\$title|\$path' genre:"$genre"`;
	$results = explode("\n", $results);
	foreach($results as $result) {
			unset($artist);
			unset($album);
			unset($title);
			unset($path);
			unset($filename);
		if (!empty($result)) {
			$params = explode("|", $result);
			// print(print_r($params, true));
			$artist = $params[0];
			$album = $params[1];
			$title = $params[2];
			$path = $params[3];
			$filename = end(explode("/", $path));

			if (!file_exists(SYNCPATH.$artist."/")) {
				print("Creating directory " . SYNCPATH.$artist."/\n");
				shell_exec("mkdir '".SYNCPATH.str_replace("'", "'\''", $artist)."/'");
			}
			if (!file_exists(SYNCPATH.$artist."/".$album."/")) {
				print("Creating directory " . SYNCPATH.$artist."/".$album."/\n");
				$album_sanitized = str_replace("/", "-", $album);
				shell_exec("mkdir '".SYNCPATH.str_replace("'", "'\''", $artist)."/".str_replace("'", "'\''", $album_sanitized)."/'");
			}
			$remote_file_path = SYNCPATH.$artist."/"."$album_sanitized"."/".$filename;
			if (empty($album) || strpos($path, "Non-Album")) {
				$remote_file_path = SYNCPATH.$artist."/".$filename;
			}
			if (false && file_exists($remote_file_path)) {
				if (md5_file($path) == md5_file($remote_file_path)) {
					print("Hashes for '$artist - $title' are identical, skipping...\n");
					continue;
				} else {
					print("Copying '$artist - $title'...\n");
					// `cp "$path" "$remote_file_path"`;
					shell_exec("cp '".str_replace("'", "'\''", $path)."' '".str_replace("'", "'\''", $remote_file_path)."'");
				}
			} else {
				// print("Copying '$artist - $title'...\n");
				shell_exec("cp '".str_replace("'", "'\''", $path)."' '".str_replace("'", "'\''", $remote_file_path)."'");
			}
		}	
	}
}

// $paths_query = `beet ls -p Yelawolf`;
//$paths_query = `beet ls -p DSB`;

// fwrite(STDIN, $paths_query);

/*$paths_split = explode("\n", $paths_query);
foreach($paths_split as $path) {
	if (!empty($path)) {
		$artist = `beet ls -f '\$artist' "$path"`;
		fwrite(STDIN, $artist);
	}
}*/

// fwrite(STDIN, print_r($firstquery, true));
// fwrite(STDIN, `beet ls Yelawolf`);
