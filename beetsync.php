#!/usr/bin/env php
<?php
require_once('vendor/autoload.php');
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

define('SOURCE_PATH', "/home/simon/Development/beetsync/");

function debug($label, $var) {
	echo $label . ": " . print_r($var, true)."\n---\n";
}

function readconfig() {
	// $config = json_decode(file_get_contents("~/.beetsync/config"));
	$config = json_decode(file_get_contents(SOURCE_PATH . "config.json"), true);
	// debug("config", $config);

	$SYNC_PATH = "";
	if (isset($config['SYNC_PATH'])) {
		if (!empty($config['SYNC_PATH'])) {
			$SYNC_PATH = $config['SYNC_PATH'];
		}
	}

	$BEETS_LIBRARY_PATH = "";
	if (isset($config['BEETS_LIBRARY_PATH'])) {
		if (!empty($config['BEETS_LIBRARY_PATH'])) {
			$BEETS_LIBRARY_PATH = $config['BEETS_LIBRARY_PATH'];
		}
	}

	define("SYNC_PATH", $SYNC_PATH);
	define("BEETS_LIBRARY_PATH", $BEETS_LIBRARY_PATH);
}

function generate(InputInterface $input, OutputInterface $output) {

	$genres = file(SOURCE_PATH . "genres");
	$artists = file(SOURCE_PATH . "artists");
	$remove_queries = file(SOURCE_PATH . "remove_queries");

	// debug("genres", $genres);
	// debug("artists", $artists);
	// debug("remove_queries", $remove_queries);

	$output->writeln('Querying for genres...');
	$genre_results = array();
	foreach ($genres as $genre) {
		$r = `beet ls -p genre:"$genre"`;
		$r = str_replace(BEETS_LIBRARY_PATH, "", $r);
		$genre_results = array_merge($genre_results, explode("\n", $r));
	}

	$output->writeln('Querying for artists...');
	$artist_results = array();
	foreach ($artists as $artist) {
		$r = `beet ls -p artist:"$artist"`;
		$r = str_replace(BEETS_LIBRARY_PATH, "", $r);
		$artist_results = array_merge($artist_results, explode("\n", $r));
	}
	

	$output->writeln('Querying for undesired tracks...');
	$remove_results = array();
	foreach ($remove_queries as $remove_query) {
		$r = `beet ls -p $remove_query`;
		$r = str_replace(BEETS_LIBRARY_PATH, "", $r);
		$remove_results = array_merge($remove_results, explode("\n", $r));
	}

	$all_files = array_unique(array_merge($genre_results, $artist_results));
	$bad_files = $remove_results;
	$good_files = array_diff($all_files, $bad_files);

	file_put_contents(SOURCE_PATH . 'rsynclist.txt', implode(PHP_EOL, $good_files));
	$output->writeln('Done generating sync file list.');
}

function transfer(InputInterface $input, OutputInterface $output) {
	if (!is_writable(SYNC_PATH)) {
		$output->writeln("Sync path: " . SYNC_PATH . "\n...is not writable! Exiting...\n");
		die();
	}
	$output->writeln("Sync path is set to " . SYNC_PATH);
	$output->writeln('Running rsync command...');

	shell_exec("rsync -a --files-from=" . SOURCE_PATH . "rsynclist.txt " . BEETS_LIBRARY_PATH . " " . SYNC_PATH);
	$output->writeln("Done transferring.");
}

function cleanup(InputInterface $input, OutputInterface $output) {
	$output->writeln("Cleanup not yet implemented!");
	die();
}

function sync(InputInterface $input, OutputInterface $output) {
	generate($input, $output);
	transfer($input, $output);
}

$app = new Application();
$app->setName("Beets Sync Tool");
$app->setVersion("0.8");

readconfig();

$app->register("generate")
	// generate the list of files
	->setDefinition(array())
	->setDescription('generate the list of files to be synced')
	->setCode("generate");

$app->register("transfer")
	// run the rsync
	->setDefinition(array())
	->setDescription('run the rsync command to transfer the music')
	->setCode("transfer");

$app->register("cleanup")
	// clean up files from sync folder that are no longer in the file list
	->setDefinition(array())
	->setDescription('clean up files in the sync folder that are no longer in the list of files')
	->setCode("cleanup");

$app->register("sync")
	// perform all actions in the sync process
	->setDefinition(array())
	->setDescription('perform all actions in the sync process at once (generate, transfer, cleanup)')
	->setCode("sync");

$app->run();
